int G =  D2;
int Y =  D3;
int R =  D4;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
 pinMode(G, OUTPUT);
 pinMode(Y, OUTPUT);
 pinMode(R, OUTPUT);
}

void loop() {
  digitalWrite(G, HIGH);
  digitalWrite(Y, HIGH);
  digitalWrite(R, HIGH);
  delay(500);
  digitalWrite(G, LOW);
  digitalWrite(Y, LOW);
  digitalWrite(R, LOW);
  delay(500);
}
