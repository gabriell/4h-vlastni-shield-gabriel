# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala - **čistý čas** |6Hodin |
| jak se mi to podařilo rozplánovat | jako jo, ale mohlo to být lepší|
| design zapojení | https://gitlab.spseplzen.cz/gabriell/4h-vlastni-shield-gabriel/-/blob/main/dokumentace/desing/XXCM7068.JPG|
| proč jsem zvolil tento design |protože se mi to tak líbilo |
| zapojení | https://gitlab.spseplzen.cz/gabriell/4h-vlastni-shield-gabriel/-/blob/main/dokumentace/desing/XXCM7068.JPG|
| z jakých součástí se zapojení skládá | Shield, kabely, rezistory, semafor, led pásek, enkodér, fotorezistor, teplotní čidlo |
| realizace | https://gitlab.spseplzen.cz/gabriell/4h-vlastni-shield-gabriel/-/blob/main/dokumentace/fotky/IMG_E9999.JPG|
| nápad, v jakém produktu vše propojit dohromady| wemos |
| co se mi povedlo | rozplánování |
| co se mi nepovedlo/příště bych udělal/a jinak | pájení |
| zhodnocení celé tvorby | 2-3 |
